package com.example.sistema.cartoes.cliente.service;

import com.example.sistema.cartoes.cliente.model.Cliente;
import com.example.sistema.cartoes.cliente.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente cadastrarCliente(Cliente cliente){
        Cliente clienteObjeto = clienteRepository.save(cliente);
        return clienteObjeto;
    }

    public Cliente buscarPorId(long id){
        Optional<Cliente> optionalCliente = clienteRepository.findById(id);
        if (optionalCliente.isPresent()){

            return optionalCliente.get();
        }
        throw new RuntimeException("O cliente não foi encontrado");
    }
}

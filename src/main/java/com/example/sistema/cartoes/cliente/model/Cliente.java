package com.example.sistema.cartoes.cliente.model;

import javax.persistence.*;

@Entity
@Table(name = "client")
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    @Column(name = "client_id")
    private long id;

    private String name;

    public Cliente() {
    }

    public Cliente(long id, String nome) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

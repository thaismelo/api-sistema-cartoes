package com.example.sistema.cartoes.cliente.controller;

import com.example.sistema.cartoes.cliente.model.Cliente;
import com.example.sistema.cartoes.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente cadastrarCliente(@RequestBody @Valid Cliente cliente) {
        try {
            Cliente clienteObjeto = clienteService.cadastrarCliente(cliente);
            return clienteObjeto;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping("/{idCliente}")
    @ResponseStatus(HttpStatus.FOUND)
    public Cliente buscarClientePorId(@PathVariable long idCliente) {
        try {
            Cliente clienteObjeto = clienteService.buscarPorId(idCliente);
            return clienteObjeto;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }
    }
}

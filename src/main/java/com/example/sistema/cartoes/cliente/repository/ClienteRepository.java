package com.example.sistema.cartoes.cliente.repository;

import com.example.sistema.cartoes.cliente.model.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Long> {
}

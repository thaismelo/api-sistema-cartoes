package com.example.sistema.cartoes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SistemaCartoesApplication {

	public static void main(String[] args) {
		SpringApplication.run(SistemaCartoesApplication.class, args);
	}

}

package com.example.sistema.cartoes.cartao.service;

import com.example.sistema.cartoes.cartao.model.Cartao;
import com.example.sistema.cartoes.cliente.model.Cliente;
import com.example.sistema.cartoes.cartao.model.dto.CartaoEntradaDTO;
import com.example.sistema.cartoes.cartao.model.dto.CartaoSaidaDTO;
import com.example.sistema.cartoes.cartao.model.dto.CartaoSaidaGetDTO;
import com.example.sistema.cartoes.cartao.model.dto.CartaoUpdateEntradaDTO;
import com.example.sistema.cartoes.cartao.repository.CartaoRepository;
import com.example.sistema.cartoes.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;


    @Autowired
    private ClienteService clientService;

    public CartaoSaidaDTO criarCartao(CartaoEntradaDTO cartaoDTO) {
        Cliente cliente = clientService.buscarPorId(cartaoDTO.getClienteId());

        Cartao cartaoObjeto = new Cartao();
        cartaoObjeto.setCliente(cliente);
        cartaoObjeto.setAtivo(false);

        Optional<Cartao> optionalNumero = cartaoRepository.findByNumero(cartaoDTO.getNumero());

        if (!optionalNumero.isPresent()) {
            cartaoObjeto.setNumero(cartaoDTO.getNumero());
            Cartao cartao = cartaoRepository.save(cartaoObjeto);

            CartaoSaidaDTO cartaoSaidaDTO = new CartaoSaidaDTO(cartao.getIdCartao(), cartao.getNumero(),
                    cliente.getId(), cartao.getAtivo());

            return cartaoSaidaDTO;
        }

        throw new RuntimeException("O cartão " + optionalNumero + " já existe.");
    }

    public CartaoSaidaDTO atualizarCartao(String numeroCartao, CartaoUpdateEntradaDTO cartaoUpdateEntradaDTO) {
        Cartao cartaoObjeto = getByNumero(numeroCartao);

        cartaoObjeto.setAtivo(cartaoUpdateEntradaDTO.getAtivo());

        Cartao cartao = cartaoRepository.save(cartaoObjeto);

        CartaoSaidaDTO cartaoSaidaDTO = new CartaoSaidaDTO(cartao.getIdCartao(), cartao.getNumero(),
                cartao.getCliente().getId(), cartao.getAtivo());

        return cartaoSaidaDTO;
    }

    public Cartao getById(Long id) {
        Optional<Cartao> byId = cartaoRepository.findById(id);

        if (byId.isPresent()) {
            return byId.get();
        }

        throw new RuntimeException("Este cartão não existe.");
    }

    public Cartao getByNumero(String numero) {
        Optional<Cartao> byNumero = cartaoRepository.findByNumero(numero);

        if (byNumero.isPresent()) {
            return byNumero.get();
        }

        throw new RuntimeException("Este número de cartão não existe.");
    }


    public CartaoSaidaGetDTO buscarPorNumero(String numero) {
        Cartao cartao = this.getByNumero(numero);

        CartaoSaidaGetDTO cartaoSaidaGetDTO = new CartaoSaidaGetDTO(cartao.getIdCartao(),
                cartao.getNumero(), cartao.getCliente().getId());

        return cartaoSaidaGetDTO;
    }


}

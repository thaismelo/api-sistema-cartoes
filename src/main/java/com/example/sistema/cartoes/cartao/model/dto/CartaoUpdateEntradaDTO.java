package com.example.sistema.cartoes.cartao.model.dto;

public class CartaoUpdateEntradaDTO {
    private Boolean ativo;

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}

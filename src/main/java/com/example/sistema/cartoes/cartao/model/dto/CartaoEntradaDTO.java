package com.example.sistema.cartoes.cartao.model.dto;

public class CartaoEntradaDTO {
    private String numero;

    private Long clienteId;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Long getClienteId() {
        return clienteId;
    }

    public void setClienteId(Long clienteId) {
        this.clienteId = clienteId;
    }

}

package com.example.sistema.cartoes.cartao.controller;

import com.example.sistema.cartoes.cartao.model.dto.CartaoEntradaDTO;
import com.example.sistema.cartoes.cartao.model.dto.CartaoSaidaDTO;
import com.example.sistema.cartoes.cartao.model.dto.CartaoSaidaGetDTO;
import com.example.sistema.cartoes.cartao.model.dto.CartaoUpdateEntradaDTO;
import com.example.sistema.cartoes.cartao.service.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CartaoSaidaDTO criarCartao(@RequestBody CartaoEntradaDTO cartaoEntradaDTO) {
        try {
            CartaoSaidaDTO cartaoSaidaDTO = cartaoService.criarCartao(cartaoEntradaDTO);
            return cartaoSaidaDTO;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PatchMapping("/{numero}")
    public CartaoSaidaDTO update(@PathVariable String numero,
                                 @RequestBody CartaoUpdateEntradaDTO cartaoEntradaUpdateDTO) {
        try {
            CartaoSaidaDTO cartaoSaidaDTO = cartaoService.atualizarCartao(numero, cartaoEntradaUpdateDTO);
            return cartaoSaidaDTO;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping("/{numero}")
    public CartaoSaidaGetDTO buscarPorNumero(@PathVariable String numero) {
        try {
            CartaoSaidaGetDTO cartaoSaidaGetDTO = cartaoService.buscarPorNumero(numero);
            return cartaoSaidaGetDTO;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}

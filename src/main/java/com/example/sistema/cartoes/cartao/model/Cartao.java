package com.example.sistema.cartoes.cartao.model;

import com.example.sistema.cartoes.cliente.model.Cliente;

import javax.persistence.*;

@Entity
@Table(name = "card")
public class Cartao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    @Column(name = "card_id")
    private long idCartao;

    @Column(name = "number")
    private String numero;

    @ManyToOne
    private Cliente cliente;

    @Column(name = "active")
    private Boolean ativo;

    public Cartao() {
    }

    public Cartao(long idCartao, String numero, Cliente cliente, Boolean ativo) {
        this.idCartao = idCartao;
        this.numero = numero;
        this.cliente = cliente;
        this.ativo = ativo;
    }

    public long getIdCartao() {
        return idCartao;
    }

    public void setIdCartao(long idCartao) {
        this.idCartao = idCartao;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}

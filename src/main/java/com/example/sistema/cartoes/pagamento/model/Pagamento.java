package com.example.sistema.cartoes.pagamento.model;

import com.example.sistema.cartoes.cartao.model.Cartao;

import javax.persistence.*;

@Entity
@Table(name = "payment")
public class Pagamento {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    @Column(name = "payment_id")
    private long idPagamento;

    @ManyToOne
    private Cartao cartao;

    @Column(name = "description")
    private String descricao;

    @Column(name = "value")
    private Double valor;

    public Pagamento() {
    }

    public Pagamento(long idPagamento, Cartao cartao, String descricao, Double valor) {
        this.idPagamento = idPagamento;
        this.cartao = cartao;
        this.descricao = descricao;
        this.valor = valor;
    }

    public long getIdPagamento() {
        return idPagamento;
    }

    public void setIdPagamento(long idPagamento) {
        this.idPagamento = idPagamento;
    }

    public Cartao getCartao() {
        return cartao;
    }

    public void setCartao(Cartao cartao) {
        this.cartao = cartao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}

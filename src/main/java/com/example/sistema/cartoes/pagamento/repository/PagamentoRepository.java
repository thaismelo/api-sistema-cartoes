package com.example.sistema.cartoes.pagamento.repository;

import com.example.sistema.cartoes.pagamento.model.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {
}
